import aiohttp
import async_timeout


class Cleverbot:
    def __init__(self, user, key):
        self.user = user
        self.key = key
        self.baseurl = "https://cleverbot.io/1.0"

    def setNick(self, nick):
        self.nick = nick

    async def create(self):
        async with aiohttp.ClientSession(connector=aiohttp.TCPConnector(verify_ssl=False)) as session:
            async with async_timeout.timeout(30):
                async with session.post(
                        '%s/create' % self.baseurl, data={
                            'user': self.user, 'key': self.key,
                            'nick': getattr(self, "nick", "")
                        }
                ) as response:
                    r = await response.json()
                    if r["status"] == "success":
                        return r["response"]
                    elif r["status"] == "Error: reference name already exists":
                        pass

    async def ask(self, question):
        async with aiohttp.ClientSession(connector=aiohttp.TCPConnector(verify_ssl=False)) as session:
            async with async_timeout.timeout(30):
                async with session.post(
                        '%s/ask' % self.baseurl, data={
                            'user': self.user, 'key': self.key,
                            'nick': getattr(self, "nick", ""), "text": question
                        }
                ) as response:
                    r = await response.json()
                    if r["status"] == "success":
                        return r["response"]
