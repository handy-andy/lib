def upper_str(arg: str):
    return arg.upper()


def find_between(s, first, last):
    start = s.index(first) + len(first)
    end = s.index(last, start)
    return s[start:end]


def platstr(argument):
    return argument.lower() if argument.lower() in ["psn", "pc", "xbox"] else "pc"
