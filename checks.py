from settings import settings


def is_owner(ctx):
    return ctx.author.id in settings["core"].bot_owner
